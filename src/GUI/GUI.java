package GUI;
import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JTextPane;
import javax.swing.ListModel;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.awt.Button;
import java.awt.Dimension;

import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import BUS.BUS;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JMenuItem;
import javax.swing.JButton;
import javax.swing.JCheckBox;

public class GUI implements IGUI {

	private JFrame frame;
	static BUS bus;
	static JList<String> list;
	static JTextArea txt_means;
	static Button but_Find;
	static JTextPane txt_message;
	static LinkedHashMap<String, Integer> dic;
	static JLabel lblTuDienAnh;
	static JCheckBox check_farvorite;
	/**
	 * Launch the application.
	 */
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frame.setVisible(true);
	
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
		File root = GetAddressFolder();
		MyResfreshDic(root);
		

	}
	public void MyResfreshDic(File root)
	{
		bus = new BUS(this, root);
	}

	private File GetAddressFolder() {
		// TODO Auto-generated method stub
		File result = new File("Anh_Viet.xml");
		return result;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 1024, 720);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnImport = new JMenu("DICTIONARIES");
		menuBar.add(mnImport);
		
		JMenuItem but_av = new JMenuItem("ANH-VIET");
		but_av.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				MyResfreshDic(new File("Anh_Viet.xml"));
				lblTuDienAnh.setText("TỪ ĐIỂN ANH VIỆT");
			}
			
		});
		mnImport.add(but_av);
		
		JMenuItem but_va = new JMenuItem("VIET-ANH");
		but_va.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				MyResfreshDic(new File("Viet_Anh.xml"));
				lblTuDienAnh.setText("TỪ ĐIỂN VIỆT ANH");

			}
			
		});
		mnImport.add(but_va);
		
		JMenuItem mntmNewMenuItem = new JMenuItem("New menu item");
		mnImport.add(mntmNewMenuItem);
		
		JMenuItem mntmNewMenuItem_1 = new JMenuItem("New menu item");
		mnImport.add(mntmNewMenuItem_1);
		
		JMenuItem mntmNewMenuItem_2 = new JMenuItem("New menu item");
		mnImport.add(mntmNewMenuItem_2);
		
		JMenu mnFavorite = new JMenu("FARVORITE");
		menuBar.add(mnFavorite);
		
		JMenuItem but_farvorite = new JMenuItem("Words");
		mnFavorite.add(but_farvorite);
		
		lblTuDienAnh = new JLabel("TỪ ĐIỂN ANH VIỆT");
		lblTuDienAnh.setHorizontalAlignment(SwingConstants.CENTER);
		lblTuDienAnh.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frame.getContentPane().add(lblTuDienAnh, BorderLayout.NORTH);
		
		Panel panel_2 = new Panel();
		frame.getContentPane().add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		Panel panel = new Panel();
		panel_2.add(panel, BorderLayout.WEST);
		panel.setLayout(new BorderLayout(0, 0));
		
		Panel panel_1 = new Panel();
		panel.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		txt_message = new JTextPane();
		panel_1.add(txt_message);
		
		but_Find = new Button("T\u00CCM");
		but_Find.setEnabled(false);
		panel_1.add(but_Find, BorderLayout.EAST);
		
		check_farvorite = new JCheckBox("Th\u00EDch");
		check_farvorite.setEnabled(false);
		check_farvorite.addChangeListener(new ChangeListener(){

			@Override
			public void stateChanged(ChangeEvent arg0) {
				// TODO Auto-generated method stub
				
				if(check_farvorite.isEnabled())
				{
					bus.changewordFarvorite(list.getSelectedValue(),check_farvorite.isSelected());
				}
				
			}});
		panel_1.add(check_farvorite, BorderLayout.NORTH);
		
		JScrollPane scrollPane = new JScrollPane();
		panel.add(scrollPane, BorderLayout.CENTER);
		
		list = new JList<String>();
		scrollPane.setViewportView(list);
		
		Dimension d = list.getPreferredSize();
		d.width = 200;
		scrollPane.setPreferredSize(d);
		
		txt_means = new JTextArea();
		txt_means.setBackground(UIManager.getColor("Button.light"));
		txt_means.setText("Nothing");
		panel_2.add(txt_means, BorderLayout.CENTER);
	}

	@Override
	public void changeListData(LinkedHashMap <String, Integer> data) {
		// TODO Auto-generated method stub
		
		dic = data;
		List<Object> lists = new LinkedList<Object>(data.entrySet());
		Iterator<Object> it = lists.iterator();
		
		DefaultListModel<String> listModel = new DefaultListModel<>();
		
		
		for (; it.hasNext();) {
			@SuppressWarnings("unchecked")
			Map.Entry<String, Integer> entry = (Entry<String, Integer>) it.next();
			listModel.addElement(entry.getKey().toString());
	    }
		list.setModel(listModel);
		list.addMouseListener(new MouseAdapter(){
			@SuppressWarnings("unchecked")
			@Override
			public void mouseClicked(MouseEvent e) {
				list = (JList<String>)e.getSource();
		        if (e.getClickCount() == 1) {
		        	txt_message.setText(list.getSelectedValue());
		        	check_farvorite.setEnabled(true);
					bus.checkKeyFarvorite(txt_message.getText().trim());
		        	bus.sendRequire(list.getSelectedValue());
		        } else if (e.getClickCount() == 3) {		            
		        }
			}
		});
		
		but_Find.setEnabled(true);
		but_Find.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				if(!txt_message.getText().trim().equals(""))
				{
					int key = getIndexMessage(dic);
					System.out.println("GIA TRI KEY: " + key);
					list.setSelectedIndex(key);
					list.ensureIndexIsVisible(key);
					bus.checkKeyFarvorite(txt_message.getText().trim());
					bus.sendRequire(txt_message.getText().trim());
				}
			}

			private int getIndexMessage(LinkedHashMap<String, Integer> dic){
				// TODO Auto-generated method stub
				try{
					return dic.get(txt_message.getText().trim());
				}catch(Exception ex)
				{
					return 0;
				}
			}});
	}

	@Override
	public void nameDictionary() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void recieveResult(String mean) {
		// TODO Auto-generated method stub
		txt_means.setText(mean);
	}

	@Override
	public void recieveFarvortie(boolean T) {
		// TODO Auto-generated method stub
		check_farvorite.setSelected(T);
	}

}
