package BUS;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;


import DAO.DAO;
import GUI.IGUI;

public class BUS implements IBUS {

	

	IGUI iGui;
	File root;
	DAO dao;
	Document data;
	static LinkedHashMap<String, String> dictionary;
	static LinkedHashMap<String, Boolean> farvorite;

	
	
	public BUS(IGUI gui, File root2) {
		// TODO Auto-generated constructor stub
		this.iGui = gui;
		this.root = root2;
		try {
			dao = new DAO(this,root);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void recieveData(Document data) {
		// TODO Auto-generated method stub
		this.data = data;
		iGui.changeListData(CreateDataMap(data));
	}

	private LinkedHashMap<String, Integer> CreateDataMap(Document data2) {
		// TODO Auto-generated method stub
		LinkedHashMap<String, Integer> result = new LinkedHashMap<String, Integer>();
		dictionary = new LinkedHashMap<String, String>();
		NodeList nlist = data2.getElementsByTagName("record");
		System.out.println("SIZE: " + nlist.getLength());
		for(int i = 0; i < nlist.getLength(); i++)
		{
			Element Ele = (Element)nlist.item(i);
			
			String key = Ele.getElementsByTagName("word").item(0).getTextContent();

			int count = 1;
			boolean f = true;
			if(result.containsKey(key))
			{
				String temp = key + " (" + String.valueOf(count) + ")";
				while(result.containsKey(temp))
				{
					count++;
					temp = key + " (" + String.valueOf(count) + ")";
				}
				key = temp;
				
				
				
				f = false;
				result.put( key, i);
				dictionary.put(key, Ele.getElementsByTagName("meaning").item(0).getTextContent());

			}
			if(f)
			{
				
				result.put( key, i);
				dictionary.put(key, Ele.getElementsByTagName("meaning").item(0).getTextContent());
			}				
		}
		return result;
	}

	public void sendRequire(String selectedValue) {
		// TODO Auto-generated method stub
		
		iGui.recieveResult(dictionary.get(selectedValue));
	}

	public void checkKeyFarvorite(String key) {
		// TODO Auto-generated method stub
		if(farvorite.containsKey(key))
		{
			try{
				if(farvorite == null)
				{
					System.out.println("NULL FARVORITE");
				}
				boolean b = farvorite.get(key);
				
				if(b)
					iGui.recieveFarvortie(true);
				else
					iGui.recieveFarvortie(false);
			}catch(Exception ex)
			{
				System.out.println(ex.toString());
				iGui.recieveFarvortie(false);
			}
		}else
			iGui.recieveFarvortie(false);
		
	}


	@Override
	public void recieveDataFarvorite(Document farvorite) {
		// TODO Auto-generated method stub
		
		this.farvorite = transFavorite(farvorite);	
	}

	private LinkedHashMap<String, Boolean> transFavorite(Document farvorite2) {
		// TODO Auto-generated method stub
		
		LinkedHashMap<String, Boolean> result = new LinkedHashMap<String, Boolean>();
		NodeList nList = farvorite2.getElementsByTagName("record");

		for(int i = 0; i < nList.getLength(); i++)
		{
			Element el = (Element) nList.item(i);
			String key = el.getElementsByTagName("word").item(0).getTextContent();
			if(!result.containsKey(key))
			{
				result.put(key, Boolean.valueOf(el.getElementsByTagName("value").item(0).getTextContent()));
			}
		}
		return result;
	}

	public void changewordFarvorite(String selectedValue, boolean selected) {
		// TODO Auto-generated method stub
		
		if(!farvorite.containsKey(selectedValue) && selected)
		{
			dao.saveNewFavoriteWord(selectedValue, selected);
			
		}
		
	}

	@Override
	public void recieveResultAddNewFavoriteWord(Element newWordFavorite) {
		// TODO Auto-generated method stub
		
		String key = newWordFavorite.getElementsByTagName("word").item(0).getTextContent();
		if(!farvorite.containsKey(key))
		{
			farvorite.put(key, Boolean.valueOf(newWordFavorite.getElementsByTagName("value").item(0).getTextContent()));
		}
		
	}

	
}
