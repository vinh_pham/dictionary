package BUS;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface IBUS {
	void recieveData(Document data);
	void recieveDataFarvorite(Document farvorite);
	void recieveResultAddNewFavoriteWord(Element newWordFavorite);
	
}
