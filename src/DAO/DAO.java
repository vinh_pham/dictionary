package DAO;

import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import BUS.IBUS;

public class DAO {

	IBUS iBus;
	File root;
	static Document docFavorite = null;
	public DAO(IBUS bus, File root) throws SAXException, IOException, ParserConfigurationException {
		// TODO Auto-generated constructor stub
		
		this.iBus = bus;
		this.root = root;
		iBus.recieveData(getData(root));
		iBus.recieveDataFarvorite(getDataFarvorite(new File("farvorite.xml")));
	}
	private Document getData(File root2) throws SAXException, IOException, ParserConfigurationException {
		// TODO Auto-generated method stub
		
		
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(root2);
        doc.getDocumentElement().normalize();	
		return doc;
	}
	
	private Document getDataFarvorite(File address) throws ParserConfigurationException, SAXException, IOException
	{
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document docFavorite = dBuilder.parse(address);
		docFavorite.getDocumentElement().normalize();
		DAO.docFavorite = docFavorite;
		return docFavorite;
	}
	public void saveNewFavoriteWord(String selectedValue, boolean selected) {
		// TODO Auto-generated method stub
		System.out.println("saveNewFavoriteWord");
		if(DAO.docFavorite.equals(null))
		{
			System.out.println("DAO.docFavorite null");
			return;
		}
		Element NewWordFavorite = DAO.docFavorite.createElement("record");
		Element node1 = DAO.docFavorite.createElement("word");
		Element node2 = DAO.docFavorite.createElement("value");
		
		
		node1.appendChild(DAO.docFavorite.createTextNode(selectedValue));
		node2.appendChild(DAO.docFavorite.createTextNode(String.valueOf(selected)));
		
		NewWordFavorite.appendChild(node1);
		NewWordFavorite.appendChild(node2);
		
		Node favor = DAO.docFavorite.getElementsByTagName("farvorite").item(0);
		
		favor.appendChild(NewWordFavorite);
				
		if(DAO.UpdateFileXML(DAO.docFavorite))
		{
			iBus.recieveResultAddNewFavoriteWord(NewWordFavorite);
		}
	}
	private static boolean UpdateFileXML(Document docFavorite2)  {
		// TODO Auto-generated method stub
		
		try{
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(DAO.docFavorite);
			StreamResult result = new StreamResult(new File("farvorite.xml"));
			transformer.transform(source, result);
			
			return true;
		}catch(TransformerException ex)
		{
			System.out.println(ex.toString());
			
			return false;
		}
		
		 
		
	}

}
